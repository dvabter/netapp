

using System.Collections.Generic;

namespace app.Models
{
  public class Client
  {

    public string         Nombre        { get; set; }
    public string         Apellido      { get; set; }
    public string         Edad          { get; set; }
    public string         Nacionalidad  { get; set; }
    public string         Sexo          { get; set; }
    public string         Apodo         { get; set; }
    public string         Telefono      { get; set; }
    public Direccion      Direccion     { get; set; }
    public List<Carrito>  Detail        { get; set; }

  }

  public class Direccion
  {
    public string Calle       { get; set; }
    public string Colonia     { get; set; }
    public string Delegacion { get; set; }
    public string Numero       { get; set; }
    public string Referencias { get; set; }
  }

  public class Carrito
  {
    public string DescripcionProducto {get; set; }
    public double Precio              { get; set; }
    public double Descuento           { get; set; }
    public int    Cantidad            { get; set; }
    public int    CantidadDisponible  { get; set; }
  }

}
