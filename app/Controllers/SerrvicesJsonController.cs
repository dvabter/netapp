using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using app.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace app.Controllers
{
    public class SerrvicesJsonController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


    public JsonResult StoresList()
    {
      //Result
      RootObject res =JsonConvert.DeserializeObject<RootObject>(JsonStores.JsonStringStores);
      return Json(res);
    }


      public JsonResult CheckOut()
        {
          return Json(new Client {
                Nombre        ="Mario",
                Apellido      ="Andrade",
                Edad          ="26",
                Nacionalidad  ="Mexicana",
                Sexo          ="Hombre",
                Apodo         ="Mayin Buu",
                Telefono      ="553269878",
                Direccion     =new Direccion {
                              Calle      ="Jalisco",
                              Colonia    ="Hidalgo",
                              Numero     ="6",
                              Referencias="Es un edificio",
                              Delegacion="Tlalpan"
                },
                Detail        = new List<Carrito> {
                  new Carrito{
                      DescripcionProducto ="Mouse",
                      Precio              =150,
                      Descuento           =0,
                      Cantidad            =1,
                      CantidadDisponible  =100
                  },
                  new Carrito{
                      DescripcionProducto ="Teclado",
                      Precio              =350,
                      Descuento           =50,
                      Cantidad            =1,
                      CantidadDisponible  =100
                  }
                }
          });
        }
  }
}
